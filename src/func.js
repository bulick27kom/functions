const getSum = (str1, str2) => {

    if (typeof(str1) !== 'string' || typeof(str2) !== 'string')
    {
      return false
    }

    if (str1.length == 0)
    {
      str1 = '0'
    }
    if (str2.length == 0)
    {
      str2 = '0'
    }


  let rez = ''

  let smallestString = str1
  let longestString = str2

  if (str1.length !== str2.length) {
     smallestString = str1.length < str2.length ? str1 : str2
     longestString = str1.length > str2.length ? str1 : str2
  }


  smallestString = smallestString.split('').reverse().join('')
  longestString = longestString.split('').reverse().join('')

  let extraSum = 0;

  for (let index = 0; index < longestString.length; index++) {
    if (index < smallestString.length) 
    {
      let tmpSum = Number(smallestString[index]) + Number(longestString[index]) + extraSum
      if (isNaN(tmpSum))
      {
        return false
      }
      if (tmpSum > 9) {
        tmpSum = tmpSum.toString()[1]
        extraSum = 1
      }
      else {
        extraSum = 0;
      }
      rez += tmpSum.toString()
    }
    else
    {
      let tmpSum = Number(longestString[index]) + extraSum
      if (tmpSum > 9) {
        tmpSum = tmpSum.toString()[1]
        extraSum = 1
      }
      else {
        extraSum = 0;
      }
      rez += tmpSum.toString()
    }    
  }
  if (extraSum > 0)
  {
    rez += extraSum
  }

  rez = rez.split('').reverse().join('')

  return rez;
};




const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0; 
  let commentsCount = 0;
  listOfPosts.forEach(post => {

    if (post.author == authorName)
    {
      postCount ++;
    }
      if (typeof(post.comments) !== 'undefined')
      {
        post.comments.forEach(comment => {
           if (comment.author == authorName)
           {
              commentsCount ++;
           }
        });        
      }
        
  });

  return 'Post:' + postCount + ',' + 'comments:' + commentsCount;
};

const tickets=(people)=> {
  let kassa = 0;
  let rez = 'YES'
  people.forEach(element => {
    element = Number(element)
    if (element == 25)
    {
      kassa += 25
    }
    else if ( (kassa - (element - 25)) < 0)
    {
      return rez = 'NO'
    }   
    else 
    {
      kassa += kassa - element + 25
    }
  });
  return rez;
};



module.exports = {getSum, getQuantityPostsByAuthor, tickets};
